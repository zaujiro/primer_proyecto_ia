package nodo;

import robot.Robot;
import tablero.Tablero;

import java.util.LinkedList;

/**
 * Clase nodo para generar el árbol de los tableros
 */
public class Nodo implements Comparable<Nodo> {
    /**
     * Estado actual del nodo
     **/
    private Tablero estado;
    /**
     * El padre del nodo
     **/
    private Nodo padre;
    /**
     * Nivel en el que se encuentra el nodo
     **/
    private int nivel;
    /**
     * Costo del nodo
     **/
    private int costo = 0;

    private boolean nodoMeta = false;
    /**
     * Hijos del nodo actual
     **/
    private LinkedList<Nodo> hijos = new LinkedList<Nodo>();

    /**
     * Constructor de la clase nodo
     *
     * @param padre
     */
    public Nodo(Tablero estado, Nodo padre, int nivel, int costo) {
        this.estado = estado;
        this.padre = padre;
        this.nivel = nivel;
        this.costo = padre != null ? costo + padre.getCosto() : costo;
    }

    /**
     * Retorna el nivel en el que se encuentra el nodo
     *
     * @return
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * Retorna el costo del nodo
     *
     * @return
     */
    public int getCosto() {
        return costo;
    }

    /**
     * Retorna el estado actual del ndo
     *
     * @return
     */
    public Tablero getEstado() {
        return estado;
    }

    /**
     * Retorna el padre del nodo actual
     *
     * @return
     */
    public Nodo getPadre() {
        return padre;
    }

    /**
     * Retorna true si es meta, false si no lo es
     *
     * @return
     */
    public boolean getMeta() {
        return nodoMeta;
    }

    /**
     * Cambia el estado del nodo para saber si ya es meta
     *
     * @param nodoMeta
     */
    public void setNodoMeta(boolean nodoMeta) {
        this.nodoMeta = nodoMeta;
    }

    /**
     * Retorna el costo si el robot que ataca es enemigo o especialidad
     *
     * @param robotAtaca robot que ataca
     * @param enemigo    robot enemigo
     * @return valor del ataque
     */
    private int getCosto(Robot robotAtaca, Robot enemigo) {
        int costo;

        /** Si el robot es es especilidad del robot que está atacando entonces su costo es de una unidad por enemigo**/
        if (robotAtaca.getEspecialidad() == enemigo) {
            costo = enemigo.getCosto() * 3;

            /** Cambiamos el valor del restraso del robot atacante**/
            robotAtaca.setRetraso(costo);

        }
        /** Por el contrario, si no es su especialidad le costará el doble derrotar a cada enemigo**/
        else {
            costo = enemigo.getCosto() * 3 * 2;

            /** Cambiamos el valor del retraso del robot atacante**/
            robotAtaca.setRetraso(costo);
        }
        return costo;
    }

    /**
     * Genera los hijos cuando los robos se encuentran en la misma columna, un robot ataca y los otros dos saltan
     *
     * @param tablero     el tablero que se va a modificar
     * @param robotAtaca  el robot que se queda atacando
     * @param robotSalta1 el robot que salta
     * @param robotSalta2 el robot que salta
     * @return
     */
    private Nodo generarNodoTresRobots(Tablero tablero, Robot robotAtaca, Robot robotSalta1, Robot robotSalta2) {

        /** Cambiamos el robot atacante a modo de ataque **/
        robotAtaca.setModoAtaque(true);

        /** Como todos se encuentran en la misma columna escogemos el enemigo que está frente a cualquier robot **/
        Robot enemigo = tablero.getMatriz()[0][robotAtaca.getCoordenada()[1] + 1];

        /** Asignamos el costo que tiene el ataque del robot a ese enemigo **/
        int costo = getCosto(robotAtaca, enemigo);


        /** Cambiar coordenadas de los robots que saltan el obstaculo **/

        moverRobotEnTablero(tablero, robotSalta1, 2);

        /** Segundo robot que salta **/
        moverRobotEnTablero(tablero, robotSalta2, 2);

        Nodo hijo = new Nodo(tablero, this, this.nivel + 1, costo);

        return hijo;
    }


    private Nodo generarNodoDosRobots(Tablero tablero, Robot robotAtaca, Robot robotSalta, Robot robotIndividual) {

        int costo = 0;
        /** Si ya el robot que ataca ya estaba en modo ataque, entonces comprobamos si el robot que saltará es aliado o enemigo **/
        if (robotAtaca.getModoAtaque()) {

            /** Obtenemos el enemigo del robot que no está en modo de ataque y va a saltar **/
            if (robotSalta.getEnemigo() == robotAtaca) {
                /** Si el robot que está atacando es enemigo, multiplicamos 2 su retraso **/
                robotAtaca.setRetraso(robotAtaca.getRetraso() * 2);
                costo = robotAtaca.getRetraso();
            } else {
                /** Si el robot que está atacando es neutral, dividimos entre 2 su retraso **/
                robotAtaca.setRetraso(robotAtaca.getRetraso() / 2);
                costo = robotAtaca.getRetraso();
            }
        } else {
            /** Cambiamos el robot atacante a modo de ataque **/
            robotAtaca.setModoAtaque(true);
            /** Comprobar si es la especialidad o enemigo del robot de la fila 1 y cambiar su restraso de tiempo **/
            Robot enemigo = tablero.getMatriz()[robotAtaca.getCoordenada()[0]][robotAtaca.getCoordenada()[1] + 1];
            /** Costo de que sea enemigo o especialidad **/
            costo = getCosto(robotAtaca, enemigo);
        }

        /** Comprobación del robot que no está en la misma fila **/
        costo += comprobarAtaqueMovimiento(tablero, robotIndividual);

        /** Cambiar coordenadas de los robots que saltan el obstaculo **/

        /** El robot que salta **/
        moverRobotEnTablero(tablero, robotSalta, 2);
        Nodo hijo = new Nodo(tablero, this, this.nivel + 1, costo);

        return hijo;

    }

    /**
     * Quita los enemigos de toda la columna
     *
     * @param tablero
     * @param robot
     */
    private void quitarEnemigos(Tablero tablero, Robot robot) {
        tablero.getMatriz()[0][robot.getCoordenada()[1] + 1] = tablero.getVacio();
        tablero.getMatriz()[1][robot.getCoordenada()[1] + 1] = tablero.getVacio();
        tablero.getMatriz()[2][robot.getCoordenada()[1] + 1] = tablero.getVacio();
    }

    /**
     * Mueve un robot en el tablero
     *
     * @param tablero el tablero en el que se va a mover
     * @param robot   el robot que se moverá
     * @param saltar  la cantidad de espacios que se va a mover
     */
    private void moverRobotEnTablero(Tablero tablero, Robot robot, int saltar) {

        tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1]] = tablero.getVacio();
        tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + saltar] = robot;

        robot.setCoordenada(robot.getCoordenada()[0], robot.getCoordenada()[1] + saltar);
    }

    /**
     * Comprueba si el robot puede avanzar o tiene un enemigo adeltante
     *
     * @param tablero
     * @param robot
     * @return
     */
    private int comprobarAtaqueMovimiento(Tablero tablero, Robot robot) {

        int costo = 0;

        /** Si el robot está en modo ataque restamos 1 a lo que tiene de retraso**/
        if (robot.getModoAtaque() && robot.getRetraso() > 0) {
            robot.setRetraso(robot.getRetraso() - 1);
            costo = robot.getRetraso();

        } else if (robot.getModoAtaque() && robot.getRetraso() == 0) {
            robot.setModoAtaque(false);
            /** Quitamos todos los enemigos **/
            quitarEnemigos(tablero, robot);
            /** Movemos al robot ***/
            moverRobotEnTablero(tablero, robot, 1);
        }
        /** Si el robot está en modo de ataque o hay un espacio vacio adeltante o la meta está adelante avanzamos **/
        else if (tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + 1] == tablero.getVacio() ||
                tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + 1] == tablero.getMeta()) {

            moverRobotEnTablero(tablero, robot, 1);

        }
        /** Si adeltante hay un enemigo cambiamos a modo ataque **/
        else if (tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + 1] != tablero.getVacio() &&
                tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + 1] != tablero.getMeta()) {

            /** Cambiamos el robot atacante a modo de ataque **/
            robot.setModoAtaque(true);
            /** Comprobar si es la especialidad o enemigo del robot de la fila 1 y cambiar su restraso de tiempo **/
            Robot enemigo = tablero.getMatriz()[robot.getCoordenada()[0]][robot.getCoordenada()[1] + 1];
            /** Costo de que sea enemigo o especialidad **/
            costo = getCosto(robot, enemigo);
        }

        return costo;
    }

    /**
     * Genera los hijos del nodo y los agrega a la listas de los hijos
     * de este nodo
     */
    public void generarHijos() {

        /**Comprobar si los 3 se encuentran en la misma columna y hay un enemigo adelante **/
        if ((estado.getRobotFila0().getCoordenada()[1] == estado.getRobotFila1().getCoordenada()[1]) &&
                (estado.getRobotFila1().getCoordenada()[1] == estado.getRobotFila2().getCoordenada()[1]) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getVacio()) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getMeta())) {


            Tablero nuevoTablero0 = new Tablero(estado);
            Tablero nuevoTablero2 = new Tablero(estado);
            Tablero nuevoTablero1 = new Tablero(estado);

            if (nuevoTablero0.getRobotFila0().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 0**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero0, nuevoTablero0.getRobotFila0(), nuevoTablero0.getRobotFila1(), nuevoTablero0.getRobotFila2()));

            } else if (nuevoTablero1.getRobotFila1().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 1**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero1, nuevoTablero1.getRobotFila1(), nuevoTablero1.getRobotFila0(), nuevoTablero1.getRobotFila2()));
            } else if (nuevoTablero1.getRobotFila1().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 2**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero2, nuevoTablero2.getRobotFila2(), nuevoTablero2.getRobotFila0(), nuevoTablero2.getRobotFila1()));
            } else {
                Tablero nuevoTablero01 = new Tablero(estado);
                /** Si se queda a pelear el robot de la fila 0**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero01, nuevoTablero01.getRobotFila0(), nuevoTablero01.getRobotFila1(), nuevoTablero01.getRobotFila2()));

                Tablero nuevoTablero11 = new Tablero(estado);

                /** Si se queda a pelear el robot de la fila 1**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero11, nuevoTablero11.getRobotFila1(), nuevoTablero11.getRobotFila0(), nuevoTablero11.getRobotFila2()));

                Tablero nuevoTablero22 = new Tablero(estado);
                /** Si se queda a pelear el robot de la fila 2**/
                this.hijos.push(generarNodoTresRobots(nuevoTablero22, nuevoTablero22.getRobotFila2(), nuevoTablero22.getRobotFila0(), nuevoTablero22.getRobotFila1()));

            }
        }

        /**Comprobar si el robot 0 y el robot 1 se  encuentran en la misma columna y hay un enemigo para decidir quien se queda **/
        else if ((estado.getRobotFila0().getCoordenada()[1] == estado.getRobotFila1().getCoordenada()[1]) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getVacio()) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getMeta())) {

            /** Si se queda a pelear el robot de la fila 0 y salta el robot 1**/
            Tablero nuevoTablero3 = new Tablero(estado);
            Tablero nuevoTablero4 = new Tablero(estado);

            if (nuevoTablero3.getRobotFila0().getModoAtaque()) {
                this.hijos.push(generarNodoDosRobots(nuevoTablero3, nuevoTablero3.getRobotFila0(), nuevoTablero3.getRobotFila1(), nuevoTablero3.getRobotFila2()));
            } else if (nuevoTablero4.getRobotFila1().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 1 y salta el robot 0**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero4, nuevoTablero4.getRobotFila1(), nuevoTablero4.getRobotFila0(), nuevoTablero4.getRobotFila2()));
            } else {

                /** Si se queda a pelear el robot de la fila 0 y salta el robot 1**/
                Tablero nuevoTablero31 = new Tablero(estado);
                this.hijos.push(generarNodoDosRobots(nuevoTablero31, nuevoTablero31.getRobotFila0(), nuevoTablero31.getRobotFila1(), nuevoTablero31.getRobotFila2()));

                /** Si se queda a pelear el robot de la fila 1 y salta el robot 0**/
                Tablero nuevoTablero41 = new Tablero(estado);
                this.hijos.push(generarNodoDosRobots(nuevoTablero41, nuevoTablero41.getRobotFila1(), nuevoTablero41.getRobotFila0(), nuevoTablero41.getRobotFila2()));
            }
        } /**Comprobar si el robot 0 y el robot 2 se  encuentran en la misma columna y hay un enemigo para decidir quien se queda **/
        else if ((estado.getRobotFila0().getCoordenada()[1] == estado.getRobotFila2().getCoordenada()[1]) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getVacio()) &&
                (estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] != estado.getMeta())) {


            Tablero nuevoTablero5 = new Tablero(estado);
            Tablero nuevoTablero6 = new Tablero(estado);

            if (nuevoTablero5.getRobotFila0().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 0 y salta el robot 2**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero5, nuevoTablero5.getRobotFila0(), nuevoTablero5.getRobotFila2(), nuevoTablero5.getRobotFila1()));

            } else if (nuevoTablero6.getRobotFila2().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 2 y salta el robot 0**/

                this.hijos.push(generarNodoDosRobots(nuevoTablero6, nuevoTablero6.getRobotFila2(), nuevoTablero6.getRobotFila0(), nuevoTablero6.getRobotFila1()));
            } else {
                /** Si se queda a pelear el robot de la fila 0 y salta el robot 2**/
                Tablero nuevoTablero51 = new Tablero(estado);
                this.hijos.push(generarNodoDosRobots(nuevoTablero51, nuevoTablero51.getRobotFila0(), nuevoTablero51.getRobotFila2(), nuevoTablero51.getRobotFila1()));


                /** Si se queda a pelear el robot de la fila 2 y salta el robot 0**/
                Tablero nuevoTablero61 = new Tablero(estado);
                this.hijos.push(generarNodoDosRobots(nuevoTablero61, nuevoTablero61.getRobotFila2(), nuevoTablero61.getRobotFila0(), nuevoTablero61.getRobotFila1()));

            }


        }
        /**Comprobar si el robot 1 y el robot 2 se  encuentran en la misma columna y hay un enemigo para decidir quien se queda **/
        else if ((estado.getRobotFila1().getCoordenada()[1] == estado.getRobotFila2().getCoordenada()[1]) &&
                (estado.getMatriz()[1][(estado.getRobotFila1().getCoordenada()[1] + 1)] != estado.getVacio()) &&
                (estado.getMatriz()[1][(estado.getRobotFila1().getCoordenada()[1] + 1)] != estado.getMeta())) {


            Tablero nuevoTablero7 = new Tablero(estado);
            Tablero nuevoTablero8 = new Tablero(estado);

            if (nuevoTablero7.getRobotFila1().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 1 y salta el robot 2**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero7, nuevoTablero7.getRobotFila1(), nuevoTablero7.getRobotFila2(), nuevoTablero7.getRobotFila0()));
            } else if (nuevoTablero7.getRobotFila1().getModoAtaque()) {
                /** Si se queda a pelear el robot de la fila 2 y salta el robot 1**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero8, nuevoTablero8.getRobotFila2(), nuevoTablero8.getRobotFila1(), nuevoTablero8.getRobotFila0()));
            } else {
                Tablero nuevoTablero71 = new Tablero(estado);

                /** Si se queda a pelear el robot de la fila 1 y salta el robot 2**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero71, nuevoTablero71.getRobotFila1(), nuevoTablero71.getRobotFila2(), nuevoTablero71.getRobotFila0()));

                Tablero nuevoTablero81 = new Tablero(estado);
                /** Si se queda a pelear el robot de la fila 2 y salta el robot 1**/
                this.hijos.push(generarNodoDosRobots(nuevoTablero81, nuevoTablero81.getRobotFila2(), nuevoTablero81.getRobotFila1(), nuevoTablero81.getRobotFila0()));

            }

        }
        /** Como ninguno se encuentra en la misma columna, comprobamos cada robot individual **/
        else {

            if ((estado.getMatriz()[0][(estado.getRobotFila0().getCoordenada()[1] + 1)] == estado.getMeta()) ||
                    (estado.getMatriz()[1][(estado.getRobotFila1().getCoordenada()[1] + 1)] == estado.getMeta()) ||
                    (estado.getMatriz()[2][(estado.getRobotFila2().getCoordenada()[1] + 1)] == estado.getMeta())) {

                Tablero nuevoTablero9 = new Tablero(estado);
                int costo = 0;
                costo += comprobarAtaqueMovimiento(nuevoTablero9, nuevoTablero9.getRobotFila0());
                costo += comprobarAtaqueMovimiento(nuevoTablero9, nuevoTablero9.getRobotFila1());
                costo += comprobarAtaqueMovimiento(nuevoTablero9, nuevoTablero9.getRobotFila2());
                Nodo hijo = new Nodo(nuevoTablero9, this, this.nivel + 1, costo);
                hijo.setNodoMeta(true);
                this.hijos.push(hijo);

            } else {

                Tablero nuevoTablero8 = new Tablero(estado);

                int costo = 0;

                costo += comprobarAtaqueMovimiento(nuevoTablero8, nuevoTablero8.getRobotFila0());
                costo += comprobarAtaqueMovimiento(nuevoTablero8, nuevoTablero8.getRobotFila1());
                costo += comprobarAtaqueMovimiento(nuevoTablero8, nuevoTablero8.getRobotFila2());

                this.hijos.push(new Nodo(nuevoTablero8, this, this.nivel + 1, costo));


            }

        }

    }


    /**
     * Retorna los hijos que tiene el nodo
     *
     * @return
     */
    public LinkedList<Nodo> getHijos() {
        return hijos;
    }

    /**
     * Ordenamos los nodos por el costo descendiente
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Nodo o) {
        return this.costo - o.getCosto();
    }
}
