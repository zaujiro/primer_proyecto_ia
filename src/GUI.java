import fileController.FileController;
import imageController.IconCellRenderer;
import nodo.Nodo;
import tablero.Tablero;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GUI {
    private JPanel panelPrincipal;
    private JButton cargarMatrizButton;
    private JLabel labelRuta;
    private JTable tableroTable;

    private FileController fileController;

    private Tablero tableroActual = null;

    /**
     * Lista de nodos Solución
     **/
    private LinkedList<Nodo> solucion = new LinkedList<Nodo>();

    public GUI() {
        cargarMatrizButton.addActionListener(ActionEvent -> openFile());
    }

    /**
     * Solicitamos la ruta del archivo donde se encuentra la getMatriz del grafo
     */
    private void openFile() {
        /**
         * Pedimos el archivo que contiene la ruta mediante una ventana
         */
        JFileChooser fileChooser = new JFileChooser();
        int selection = fileChooser.showOpenDialog(panelPrincipal);

        /**
         * Si se seleccionó se crea el archivo con la clase auxiliar FileCOntroller
         */
        if (selection == JFileChooser.APPROVE_OPTION) {
            fileController = new FileController(fileChooser.getSelectedFile());
            labelRuta.setText(fileController.getRuta());

            /**
             * Creamos el tableroTable que estará en el juego
             */

            Tablero tablero = new Tablero(leerArchivo());
            tableroActual = tablero;

            /** Llenamos la tabla inicial ***/
            llenarTabla(tableroActual);

            /** Ejecución de la tarea cada n segundos**/
            ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
            timer.scheduleAtFixedRate(tarea, 1, 3, TimeUnit.SECONDS);
        }
    }

    /**
     * obtiene el contenido del archivo que se ha cargado
     * y retorna un string  que es la getMatriz
     *
     * @return la getMatriz en string
     */
    private String leerArchivo() {
        return fileController.getFileContent();
    }

    /**
     * Retorna el panel actual
     *
     * @return el panel actual
     */
    public JPanel getPanelPrincipal() {
        return panelPrincipal;
    }


    /**
     * Llena el Jtable que mostrará el tablero en el que se moverán los robots
     *
     * @param tablero el tablero que contiene la getMatriz
     */
    public void llenarTabla(Tablero tablero) {

        tableroTable.setDefaultRenderer(Object.class, new IconCellRenderer());
        DefaultTableModel modelo = (DefaultTableModel) tableroTable.getModel();

        /**
         * Indicamos cuantas getNumeroFilas y getNumeroColmnas tendrá la tabla
         */
        modelo.setRowCount(tablero.getNumeroFilas()); // cantidad total de getNumeroFilas
        modelo.setColumnCount(tablero.getNumeroColmnas()); // cantidad total de getNumeroColmnas

        System.out.println(" Columnas: "+tablero.getNumeroColmnas());
        /**
         * Llenamos cada espacio de la tabla con los datos de la getMatriz
         */
        for (int i = 0; i < tablero.getNumeroFilas(); i++) {
            for (int j = 0; j < tablero.getNumeroColmnas(); j++) {
                ImageIcon img = new ImageIcon(getClass().getResource(tablero.getMatriz()[i][j].getImg()));
                tableroTable.setValueAt(new JLabel(img), i, j);
                tableroTable.setRowHeight(80);
            }
        }
        /** Llamada al método de costo uniforme ***/
        costoUniforme(tablero);
    }


    public void cambiarTabla(Tablero tablero) {
        /**
         * Llenamos cada espacio de la tabla con los datos de la getMatriz
         */
        for (int i = 0; i < tablero.getNumeroFilas(); i++) {
            for (int j = 0; j < tablero.getNumeroColmnas(); j++) {
                ImageIcon img = new ImageIcon(getClass().getResource(tablero.getMatriz()[i][j].getImg()));
                tableroTable.setValueAt(new JLabel(img), i, j);
            }
        }
    }


    /**
     * Lee la solución del nodo y cambia el tablero
     */
    private void leerSolucion() {
        if (solucion.size() > 0) {
            Nodo nodo = solucion.pop();
            cambiarTabla(nodo.getEstado());
        }
    }

    /**
     * Construye la solución que se encontró por costo uniforme
     * lee el nodo meta y recorre el padre para ingresarlos en la lista de solución
     * @param nodo
     */
    private void construirSolucion(Nodo nodo) {

        Nodo actual = nodo;

        /** Leemos hasta que el último nodo no tenga padre **/
        while (actual.getPadre() != null) {
            /** Enviamos el nodo a la lista de soluciones**/
            solucion.push(actual);
            actual = actual.getPadre();
        }
    }

    /**
     * Hilo para la ejecución de la tarea cada n segundos
     **/
    final Runnable tarea = new Runnable() {
        public void run() {
            System.out.println("* Ejecutar cambiar tabla");
            leerSolucion();
        }
    };

    private void mostrarNodo(Nodo nodo) {

        System.out.println("-- Mostrar NODO -- ");
        System.out.println("  Nodo Nivel: " + nodo.getNivel() + " | Hijos: " + nodo.getHijos().size() + " | Costo: " + nodo.getCosto() + " | Meta: " + nodo.getMeta());
    }

    private void mostrarHijosNodo(Nodo nodo) {
        System.out.println("-- Hijos del NODO -- ");
        for (int i = 0; i < nodo.getHijos().size(); i++) {
            System.out.println("  *  # " + i + "  Nivel: " + nodo.getHijos().get(i).getNivel() + " Costo: " + nodo.getHijos().get(i).getCosto());
            nodo.getHijos().get(i).getEstado().mostrarTablero();
        }
    }

    /**
     * Aplica la busqueda por costo uniforme
     *
     * @param tablero
     */
    public void costoUniforme(Tablero tablero) {

        /** El primer tablero es el nodo raiz **/
        Nodo raiz = new Nodo(tablero, null, 0, 0);
        raiz.generarHijos();

        /** Obtenemos los hijos de la raiz **/
        LinkedList<Nodo> hijos = raiz.getHijos();

        /** Lista de nodos expandidos **/
        LinkedList<Nodo> explorados = new LinkedList<Nodo>();

        /**
         * Leemos los hijos hasta que se termine la lista
         */
        while (hijos.size() > 0) {

            /**Ordenamos los hijos de acuerdo al costo en cada iteración  de menor a mayor**/
            Collections.sort(hijos);

            /** Obtenemos el primer nodo que tiene el costo más bajo **/
            Nodo actual = hijos.pop();

            /** Comprobamos si el estado actual es la meta **/
            if (actual.getMeta()) {

                System.out.println("-- Solución --");
                System.out.println("Nivel: "+actual.getNivel()+" | costo: "+actual.getCosto());
                /** Llamamos al método que construye la solución **/
                construirSolucion(actual);
                /** Enviamos el actual a la lista de explorados **/
                explorados.push(actual);

                break;
            }
            /** Si el estado actual aún no es la meta, agregamos el actual a la lista de explorados y agregamos sus hijos a la lista enlazada
             * y la ordenamos de mayor a menor **/
            else {

                /** Generamos los hijos del nodo actual **/
                actual.generarHijos();
                /** Enviamos el nodo actual a la lista de explorados **/
                explorados.push(actual);
                /** Agregamos todos los hijos del actual a la listas de hijos**/
                hijos.addAll(actual.getHijos());
            }
        }

        /*** Si se terminan los hijos y no encontró una solución es porque el tablero no tiene solución por costo uniforme **/
        if (hijos == null) {
            System.out.println("SE TERMINARON LOS HIJOS, NO TIENE SOLUCIÓN");
        }

    }
}
