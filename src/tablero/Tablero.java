package tablero;

import robot.Robot;

public class Tablero {

    private Robot[][] matriz;
    private String[] filas;
    private String[] columnas;

    /**
     * Personajes que se pueden mover
     **/
    private Robot piedra = null;
    private Robot papel = null;
    private Robot tijera = null;

    /**
     * Personajes estaticos, nunca se mueven de su posición
     **/
    private Robot brocoli = null;
    private Robot ladron = null;
    private Robot homero = null;
    private Robot vacio = null;
    private Robot meta = null;

    /**
     * Robots por fila
     **/
    private Robot robotFila0 = null;
    private Robot robotFila1 = null;
    private Robot robotFila2 = null;

    /**
     * Constructor de la clase Tablero
     *
     * @param tablero
     */
    public Tablero(String tablero) {
        /**
         * Creamos los robots que puede tener el tablero
         */
        /** Representación de los jugadores del tablero **/
        this.piedra = new Robot('R',"Piedra", "img/piedra.png", null, null, 0,false);
        this.papel = new Robot('P',"Papel", "img/papel.png", null, null, 0,false);
        this.tijera = new Robot('T',"Tijera", "img/tijera.png", null, null, 0,false);
        /** Representación de los enemigos del tablero **/
        this.brocoli = new Robot('B',"Brocoli", "img/brocoli.png", null, null, 1,false);
        this.ladron = new Robot('L',"Ladron", "img/ladron.png", null, null, 1,false);
        this.homero = new Robot('H',"Homero", "img/homero.png", null, null, 1,false);
        /** Representación de la meta y el espacio vacio **/
        this.vacio = new Robot('-',"-", "img/vacio.png", null, null, 0,false);
        this.meta = new Robot('*',"*", "img/meta.png", null, null, 0,false);

        /**
         * leemos cada fila de la mátriz
         */
        filas = tablero.split("\n");
        columnas = filas[0].split(" ");

        /**
         * Como NO es una mátriz cuadrada debemos tomar el número de getNumeroFilas y getNumeroColmnas por aparte
         */
        this.matriz = new Robot[filas.length][columnas.length];

        /**
         * recorremos la getNumeroFilas
         */
        for (int f = 0; f < filas.length; f++) {
            /**
             * Ahora que tenemos cada fila, leemos cada valor de las getNumeroColmnas separado por un espacio
             */
            columnas = filas[f].split(" ");

            /**
             * recorremos la columna de cada fila
             */
            for (int k = 0; k < columnas.length; k++) {

                /**
                 * Comenzamos a llenar la getMatriz con los valores
                 */
                actualizarPosicion(f, k, objetoTablero(columnas[k]));
            }
        }
    }

    public Tablero(Tablero tablero) {

        this.filas = tablero.getFilas();
        this.columnas = tablero.getColumnas();

        /** Enemigos **/
        this.brocoli = tablero.getBrocoli();
        this.ladron = tablero.getLadron();
        this.homero = tablero.getHomero();

        /** Vacio y meta **/
        this.vacio = tablero.getVacio();
        this.meta = tablero.getMeta();


        /** Robots en la fila **/
        this.robotFila0 = new Robot(tablero.getRobotFila0().getAbreviatura(),tablero.getRobotFila0().getNombre(), tablero.getRobotFila0().getImg(), tablero.getRobotFila0().getEnemigo(), tablero.getRobotFila0().getEspecialidad(), tablero.getRobotFila0().getCosto(),tablero.getRobotFila0().getModoAtaque());
        this.robotFila1 = new Robot(tablero.getRobotFila1().getAbreviatura(),tablero.getRobotFila1().getNombre(), tablero.getRobotFila1().getImg(), tablero.getRobotFila1().getEnemigo(), tablero.getRobotFila1().getEspecialidad(), tablero.getRobotFila1().getCosto(),tablero.getRobotFila1().getModoAtaque());
        this.robotFila2 = new Robot(tablero.getRobotFila2().getAbreviatura(),tablero.getRobotFila2().getNombre(), tablero.getRobotFila2().getImg(), tablero.getRobotFila2().getEnemigo(), tablero.getRobotFila2().getEspecialidad(), tablero.getRobotFila2().getCosto(),tablero.getRobotFila2().getModoAtaque());

        this.robotFila0.setCoordenada(tablero.getRobotFila0().getCoordenada()[0],tablero.getRobotFila0().getCoordenada()[1]);
        this.robotFila1.setCoordenada(tablero.getRobotFila1().getCoordenada()[0],tablero.getRobotFila1().getCoordenada()[1]);
        this.robotFila2.setCoordenada(tablero.getRobotFila2().getCoordenada()[0],tablero.getRobotFila2().getCoordenada()[1]);

        this.robotFila0.setRetraso(tablero.getRobotFila0().getRetraso());
        this.robotFila1.setRetraso(tablero.getRobotFila1().getRetraso());
        this.robotFila2.setRetraso(tablero.getRobotFila2().getRetraso());
        /** Matriz de robots**/
        this.matriz = new Robot[tablero.getNumeroFilas()][tablero.getNumeroColmnas()];
        /**
         * recorremos la getNumeroFilas
         */
        for (int f = 0; f < filas.length; f++) {
            /**
             * recorremos la columna de cada fila
             */
            for (int k = 0; k < columnas.length; k++) {
                /**
                 * Comenzamos a llenar la getMatriz con los valores
                 */
                this.matriz[f][k] = tablero.getMatriz()[f][k];
            }
        }
    }

    /**
     * Método para actualizar el valor de una ruta
     *
     * @param fila    fila de la getMatriz
     * @param columna columna de la getMatriz
     * @param robot   valor en la posición (fila,columna)
     */
    private void actualizarPosicion(int fila, int columna, Robot robot) {
        /** Ubicamos el robot en la coordenada **/
        this.matriz[fila][columna] = robot;
        /** Actualizamos las coordenadas del robot para cuando lo consultemos sepamos cual es su posición actual **/
        robot.setCoordenada(fila, columna);

        /** Comprobamos los robots que quedan en cada fila **/

        if (robot == this.piedra) {
            if (fila == 0) {
                robotFila0 = robot;
            } else if (fila == 1) {
                robotFila1 = robot;
            } else if (fila == 2) {
                robotFila2 = robot;
            }
        } else if (robot == this.papel) {
            if (fila == 0) {
                robotFila0 = robot;
            } else if (fila == 1) {
                robotFila1 = robot;
            } else if (fila == 2) {
                robotFila2 = robot;
            }
        } else if (robot == this.tijera) {
            if (fila == 0) {
                robotFila0 = robot;
            } else if (fila == 1) {
                robotFila1 = robot;
            } else if (fila == 2) {
                robotFila2 = robot;
            }
        }
    }

    /**
     * Muestra la getMatriz que se llenó con el archivo de texto que se cargó
     */
    public void mostrarTablero() {
        /***
         * Leemos las getNumeroFilas de la getMatriz
         */
        for (int i = 0; i < filas.length; i++) {
            /** Leemos las getNumeroColmnas de la getMatriz */
            for (int j = 0; j < columnas.length; j++) {
                /**Mostramos el valor en la posición (i,j) **/
                System.out.print(matriz[i][j].getAbreviatura() + " ");
            }
            /** Salto de linea para cada fila  */
            System.out.print("\n");
        }
    }


    /**
     * retorna la getMatriz que se llenó con los datos del txt
     *
     * @return una getMatriz de string
     */
    public Robot[][] getMatriz() {
        return matriz;
    }

    public String[] getFilas() {
        return filas;
    }

    public String[] getColumnas() {
        return columnas;
    }

    /**
     * Retorna la cantidad de getNumeroFilas de la getMatriz
     *
     * @return
     */
    public int getNumeroFilas() {
        return filas.length;
    }

    /**
     * retorna la cantidad de getNumeroColmnas de la getMatriz
     *
     * @return
     */
    public int getNumeroColmnas() {
        return columnas.length;
    }

    /**
     * Retorna un objeto dependiendo del valor que se pusiera en la getMatriz
     *
     * @param valor el valor que recibe
     * @return el objeto al que fue convertido
     */
    private Robot objetoTablero(String valor) {


        Robot papel = new Robot('P',"Papel", "img/papel.png", this.tijera, this.homero, 0,false);
        Robot piedra = new Robot('R',"Piedra", "img/piedra.png", this.papel, this.ladron, 0,false);
        Robot tijera = new Robot('T',"Tijera", "img/tijera.png", this.piedra, this.brocoli, 0,false);

        Robot robot = null;

        /**
         * Si es P(Paper), creamos el robot piedra
         */
        if (valor.equals("P")) {
            this.papel = papel;
            robot = this.papel;
        }
        /**
         * Si es R(Rock), creamos el robot piedra
         */
        else if (valor.equals("R")) {
            this.piedra = piedra;
            robot = this.piedra;
        }
        /**
         * Si es S(Scissors), creamos el robot tijera
         */
        else if (valor.equals("S")) {
            this.tijera = tijera;
            robot = this.tijera;
        }
        /**
         * Si es B (brocoli), es un enemigo
         */
        else if (valor.equals("B")) {
            robot = brocoli;
        }
        /**
         * Si es L (ladrón), es un enemigo
         */
        else if (valor.equals("L")) {
            robot = ladron;
        }
        /**
         * Si es H (Homero), es un enemigo
         */
        else if (valor.equals("H")) {
            robot = homero;
        }
        /**
         * Si s 1, es la meta
         */
        else if (valor.equals("1")) {
            robot = meta;
        }
        /**
         * Si no es ninguno de los anteriores entonces es una casilla vacia
         */
        else {
            robot = vacio;
        }

        return robot;
    }

    public Robot getBrocoli() {
        return brocoli;
    }

    public Robot getLadron() {
        return ladron;
    }

    public Robot getHomero() {
        return homero;
    }

    public Robot getVacio() {
        return vacio;
    }

    public Robot getMeta() {
        return meta;
    }

    /**
     * Retorna el robot de la fila 1
     *
     * @return
     */
    public Robot getRobotFila0() {
        return robotFila0;
    }

    /**
     * Retorna el robot de la fila 1
     *
     * @return
     */
    public Robot getRobotFila1() {
        return robotFila1;
    }

    /**
     * Retorna el robot de la fila 1
     *
     * @return
     */
    public Robot getRobotFila2() {
        return robotFila2;
    }

}
