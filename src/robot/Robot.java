package robot;

public class Robot {
    /**
     * Nombre del robot
     **/
    private String nombre;
    /**
     * Enemigo del robot
     **/
    private Robot enemigo;
    /**
     * Enemigo que derrota fácilmente
     **/
    private Robot especialidad;
    /**
     * Imagen del robot
     **/
    private String img;
    /**
     * Coordenadas del robot, posición actual
     **/
    private int coordenada[] = {0, 0};

    /**
     * costo de pelear contra este robot
     **/
    private int costo = 0;

    /**
     * retraso del robot
     **/
    private int retraso = 0;

    /**
     * Indica si el robot está o no atacando
     **/
    private boolean modoAtaque;

    /** Abreviatura del robot **/
    private char abreviatura;

    /**
     * Constructor de la clase Robot
     *
     * @param nombre       Nombre del robot
     * @param img          ruta de la imagen del robot
     * @param enemigo      enemigo del robot
     * @param especialidad obstaculo que derrota más fácilmente
     */
    public Robot(char abreviatura, String nombre, String img, Robot enemigo, Robot especialidad, int costo, boolean modoAtaque) {
        this.abreviatura = abreviatura;
        this.nombre = nombre;
        this.img = img;
        this.enemigo = enemigo;
        this.especialidad = especialidad;
        this.costo = costo;
        this.modoAtaque = modoAtaque;
    }

    /**
     * Retorna la abreviatura del robot
     *
     * @return
     */
    public char getAbreviatura() {
        return abreviatura;
    }

    /**
     * Retorna la ruta de la imagen del robot
     *
     * @return ruta de la imagen del robot
     */
    public String getImg() {
        return img;
    }

    /**
     * Retorna el nombre del robot
     *
     * @return nombre del robot
     */
    public String getNombre() {
        return nombre;

    }

    /**
     * Retorna el enemigo del robot
     *
     * @return el enemigo del robot
     */
    public Robot getEnemigo() {
        return enemigo;
    }

    /**
     * Retorna la especialidad del robot
     *
     * @return el obstaculo que derrota más fácil
     */
    public Robot getEspecialidad() {
        return especialidad;
    }

    /**
     * Retorna el costo en tiempo que tiene el robot actualmente
     *
     * @return
     */
    public int getCosto() {
        return costo;
    }


    /**
     * Retorna el retraso que tiene el robot
     *
     * @return
     */
    public int getRetraso() {
        return retraso;
    }

    /**
     * Retorna si el robot está en modo ataque
     * @return
     */
    public boolean getModoAtaque() {
        return modoAtaque;
    }

    /**
     * Actualiza el valor del modo ataque
     * @param modoAtaque
     */
    public void setModoAtaque(boolean modoAtaque) {
        this.modoAtaque = modoAtaque;
    }

    /**
     * Actualiza el valor del retraso del robot
     *
     * @param retraso
     */
    public void setRetraso(int retraso) {
        this.retraso = retraso;
    }

    /**
     * Retorna la posición actual del robot en la getMatriz del tablero
     *
     * @param fila    coordenada en columna
     * @param columna coordenada en columna
     */
    public void setCoordenada(int fila, int columna) {

        coordenada[0] = fila;
        coordenada[1] = columna;
    }

    /**
     * Retorna la coordenada del robot
     *
     * @return
     */
    public int[] getCoordenada() {
        return coordenada;
    }
}
